//Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//DOM - модель документа, с которым работает браузер, и которую можно изменять посредством доступа к его методам.
//


const elementUl = document.createElement('ul');
const listExample = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
document.body.appendChild(elementUl);

const data = function (arr) {
  const container = document.getElementsByTagName('ul')[0];
  container.innerHTML = arr.map((element) => `<li>${element}</li>`).join('');
};

data(listExample);
